(* ocamlfind ocamlc -o test -package oUnit -linkpkg -g OverlappingClaims.ml OverlappingClaimsTest.ml *)

open OUnit;;
open OverlappingClaims;;

(* Utility functions *)
let string_of_integerTuple (i1, i2) = "(" ^ (string_of_int i1) ^ ", " ^ (string_of_int i2) ^ ")";;
let string_of_stringTuple (s1, s2, s3) = "(" ^ s1 ^ ", " ^ s2 ^ ", " ^ s3 ^ ")";;

let string_of_input input =
	"[ID: " ^ (string_of_int input.id) ^ "| " ^
	"topLeft: " ^ (string_of_integerTuple input.topLeft) ^ "| " ^
	"dimension: " ^ (string_of_integerTuple input.dimensions) ^ "]";;

let rec string_of_inputList outputString inputList = match inputList with
	| [] -> outputString
	| input::inputListTail -> string_of_inputList (outputString ^ (string_of_input input)) inputListTail;;

let rec string_of_integerList intList = match intList with
	| [] -> " end"
	| i::intListTail -> string_of_int i ^ " ; " ^ (string_of_integerList intListTail);;

let string_of_point point = match point with
	| TL(a,b) -> "TL" ^ string_of_integerTuple (a,b)
	| TR(a,b) -> "TR" ^ string_of_integerTuple (a,b)
	| BL(a,b) -> "BL" ^ string_of_integerTuple (a,b)
	| BR(a,b) -> "BR" ^ string_of_integerTuple (a,b);;

let string_of_rectangle (R(p1, p2, p3, p4)) = "R [" ^
	string_of_point p1 ^ ", " ^
	string_of_point p2 ^ ", " ^
	string_of_point p3 ^ ", " ^
	string_of_point p4 ^ "]";;

let rec string_of_rectangleList rectangleList = match rectangleList with
	| [] -> " end"
	| r::rectangleListTail -> string_of_rectangle r ^ " ; " ^ (string_of_rectangleList rectangleListTail);;

(* Test Suite *)
let test_suite = "OverlappingClaimsTest" >:::
[
	(* OverlappingClaims.parseInputString *)
	"givenInputString_shouldParseCorrectly" >:: ( fun() ->
		assert_equal
			~printer: string_of_input {id=123; topLeft=(0,0); dimensions=(1,1)}
			(OverlappingClaims.parseInputString "#123 @ 0,0: 1x1");
	);

	(* OverlappingClaims.extractValues *)
	"givenInputString_shouldExtractEachValue" >:: ( fun() ->
		assert_equal
			~printer: string_of_stringTuple ("#123", "0,0", "1x1")
			(OverlappingClaims.extractValues "#123 @ 0,0: 1x1");
	);

	(* OverlappingClaims.extractId *)
	"givenIdString_shouldExtractId" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 123
			(OverlappingClaims.extractId "#123");
	);

	(* OverlappingClaims.extractTopLeftPoint *)
	"givenTopLeftString_shouldExtractTopLeftPoint" >:: ( fun() ->
		assert_equal
			~printer: string_of_integerTuple (0,0)
			(OverlappingClaims.extractTopLeftPoint "0,0");
	);

	(* OverlappingClaims.extractDimensions *)
	"givenDimensionsString_shouldExtractDimensions" >:: ( fun() ->
		assert_equal
			~printer: string_of_integerTuple (1,1)
			(OverlappingClaims.extractDimensions "1x1");
	);

	(* OverlappingClaims.calculateArea *)
	"givenDimensions_shouldCalculateArea" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 6
			(OverlappingClaims.calculateArea (2,3));
	);

	(* OverlappingClaims.parseInput *)
	"givenInput_shouldParseCorrectly" >:: ( fun() ->
		assert_equal
			~printer: (string_of_inputList "") [{id=1; topLeft=(1,3); dimensions=(4,4)}; {id=2; topLeft=(3,1); dimensions=(4,4)}; {id=3; topLeft=(5,5); dimensions=(2,2)}]
			(OverlappingClaims.parseInput "test-input.txt");
	);

	(* OverlappingClaims.calculateAreas *)
	"givenInputList_shouldExtractAreas" >:: ( fun() ->
		assert_equal
			~printer: string_of_integerList [16;12]
			(OverlappingClaims.calculateAreas [{id=1; topLeft=(1,3); dimensions=(4,4)}; {id=2; topLeft=(3,1); dimensions=(4,3)}] []);
	);

	(* OverlappingClaims.calculateTotalArea *)
	"givenInputList_shouldCalculateTotalArea" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 28
			(OverlappingClaims.calculateTotalArea [{id=1; topLeft=(1,3); dimensions=(4,4)}; {id=2; topLeft=(3,1); dimensions=(4,3)}]);
	);

	(* OverlappingClaims.convertToRectangle *)
	"givenInput_shouldConvertToRectangle" >:: ( fun() ->
		assert_equal
			~printer: string_of_rectangle (R(TL(1,3), TR(4,3), BL(1,6), BR(4,6)))
			(OverlappingClaims.convertToRectangle {id=1; topLeft=(1,3); dimensions=(4,4)});
	);

	(* OverlappingClaims.convertToRectangleList *)
	"givenInputList_shouldConvertToRectangleList" >:: ( fun() ->
		assert_equal
			~printer: string_of_rectangleList [(R(TL(1,3), TR(4,3), BL(1,6), BR(4,6))); (R(TL(3, 1), TR(6, 1), BL(3, 3), BR(6, 3)))]
			(OverlappingClaims.convertToRectangleList [{id=1; topLeft=(1,3); dimensions=(4,4)};{id=2; topLeft=(3,1); dimensions=(4,3)}] []);
	);
];;

let _ = run_test_tt_main test_suite;;
