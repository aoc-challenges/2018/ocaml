(* ocamlc -o overlappingClaims OverlappingClaims.ml *)

open List;;

(* Types *)
type input = {id:int; topLeft:int * int; dimensions:int * int};;

type point =
	TL of int * int
	| TR of int * int
	| BL of int * int
	| BR of int * int;;

type rectangle =
	| R of point * point * point * point;;

(* Utility *)
let read_file filename =
	let lines = ref [] in
	let chan = open_in filename in
	try
	  while true; do
		lines := input_line chan :: !lines
	  done; !lines
	with End_of_file ->
	  close_in chan;
	  List.rev !lines;;

let extractValues inputString =
	match String.split_on_char '@' inputString with
		| id::[therest] ->
			(match String.split_on_char ':' therest with
				| point::[dim] -> (String.trim id, String.trim point, String.trim dim)
				| _ -> failwith "Unexpected result")
		| _ -> failwith "Unexpected result";;

let extractId idString =
	match String.split_on_char '#' idString with
		| _::[id] -> int_of_string id
		| _ -> failwith "Unexpected result";;

let extractTopLeftPoint topLeftString =
	match String.split_on_char ',' topLeftString with
		| x::[y] -> (int_of_string x, int_of_string y)
		| _ -> failwith "Unexpected result";;

let extractDimensions dimensionsString =
	match String.split_on_char 'x' dimensionsString with
		| length::[width] -> (int_of_string length, int_of_string width)
		| _ -> failwith "Unexpected result";;

let parseInputString inputString =
	let (id, topLeft, dimensions) = extractValues inputString in
	{id=extractId id; topLeft=extractTopLeftPoint topLeft; dimensions=extractDimensions dimensions};;

let rec makeInputList inputStringList output = match inputStringList with
	| [] -> output
	| inputString::inputStringListTail -> makeInputList inputStringListTail (output@[parseInputString inputString]);;

let parseInput filename =
	let inputStrings = read_file filename in
	makeInputList inputStrings [];;

(* Helpers *)
let calculateArea (length, width) = length * width;;

let rec calculateAreas parsedInput output = match parsedInput with
	| [] -> output
	| input::parsedInputTail ->
		let area = calculateArea input.dimensions in
		calculateAreas parsedInputTail (output@[area]);;

let rec sumAll (numberList:int list) (sum:int): int = match numberList with
	| [] -> sum
	| n::numberListTail -> sumAll numberListTail (sum + n);;

let calculateTotalArea parsedInput =
	sumAll (calculateAreas parsedInput []) 0;;

let convertToRectangle ({id=_; topLeft=(x, y); dimensions=(l, w)}) =
	let xChange = l-1 in
	let yChange = w-1 in
	R(TL(x,y), TR(x+xChange,y), BL(x,y+yChange), BR(x+xChange,y+yChange));;

let rec convertToRectangleList inputList rectangleList = match inputList with
	| [] -> rectangleList
	| i::inputListTail -> convertToRectangleList inputListTail (rectangleList@[convertToRectangle i]);;
