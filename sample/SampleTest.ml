open OUnit;;

let tests = "SampleTests" >:::
[
	"add_numbers" >:: ( fun() ->
		assert_equal 
			~printer: string_of_int 4
			(Sample.add 1 3);
	);
	"add_three_to_number" >:: ( fun() ->
		assert_equal 
			~printer: string_of_int 5
			(Sample.addThree 2);
	)
];;

let _ = run_test_tt_main tests;;