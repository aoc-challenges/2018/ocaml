(* ocamlfind ocamlc -o test -package oUnit -linkpkg -g ManageInventory.ml ManageInventoryTest.ml *)

open OUnit;;
open String;;

(* Utility functions *)
let rec printList string list = match list with
	| [] -> "end"
	| [c] -> (make 1 c) ^ " end"
	| c::list ->
		let charAsString = (make 1 c) in
		charAsString ^ " " ^ (printList (charAsString ^ " " ^ string) list)

let rec printIntegerList string list = match list with
	| [] -> "end"
	| [n] -> (string_of_int n) ^ " end"
	| n::list ->
		let intAsString = (string_of_int n) in
		intAsString ^ " " ^ (printIntegerList (intAsString ^ " " ^ string) list)

let printIntegerTuple (i1, i2) = "(" ^ (string_of_int i1) ^ ", " ^ (string_of_int i2) ^ ")"

let printCharListIntegerListTuple (cl, il) = "(" ^ (printList "" cl) ^ ", " ^ (printIntegerList "" il) ^ ")"

(* Test Suite *)
let test_suite = "ManageInventoryTest" >:::
[
	(* ManageInventory.countLetters *)
	"givenEmptyList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.countLetters 'a' [] 0);
	);
	"givenSingletonList_shouldReturnOne" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 1
			(ManageInventory.countLetters 'a' ['a'] 0);
	);
	"givenSingletonList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.countLetters 'a' ['b'] 0);
	);
	"givenCharacterList_shouldReturnTwo" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 2
			(ManageInventory.countLetters 'a' ['b';'a';'a'] 0);
	);
	"givenCharacterList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.countLetters 'a' ['b';'c';'d'] 0);
	);

	(* ManageInventory.removeDuplicateLetters *)
	"givenEmptyList_shouldReturnEmptyList" >:: ( fun() ->
		assert_equal
			~printer: (printList "") []
			(ManageInventory.removeDuplicateLetters []);
	);
	"givenSingletonList_shouldReturnSameList" >:: ( fun() ->
		assert_equal
			~printer: (printList "") ['a']
			(ManageInventory.removeDuplicateLetters ['a']);
	);
	"givenCharacterList_shouldReturnSameList" >:: ( fun() ->
		assert_equal
			~printer: (printList "") ['a';'b']
			(ManageInventory.removeDuplicateLetters ['a';'b']);
	);
	"givenCharacterList_shouldReturnListWithoutDuplicates" >:: ( fun() ->
		assert_equal
			~printer: (printList "") ['a';'b']
			(ManageInventory.removeDuplicateLetters ['a';'b';'a';'b';'a']);
	);

	(* ManageInventory.countEachLetter *)
	"givenEmptyList_shouldReturnEmptyList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") []
			(ManageInventory.countEachLetter [] ['a'] []);
	);
	"givenSingletonList_shouldReturnZeroList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [0]
			(ManageInventory.countEachLetter ['a'] ['b'] []);
	);
	"givenSingletonList_shouldReturnOneList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [1]
			(ManageInventory.countEachLetter ['a'] ['a'] []);
	);
	"givenEmptyLetterList_shouldReturnZeroList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [0;0]
			(ManageInventory.countEachLetter ['a';'b'] [] []);
	);
	"givenSingletonLetterList_shouldReturnZeroList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [0;0]
			(ManageInventory.countEachLetter ['a';'b'] ['c'] []);
	);
	"givenSingletonLetterList_shouldReturnOneAndZeroList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [0;1]
			(ManageInventory.countEachLetter ['a';'b'] ['b'] []);
	);
	"givenCharacterLetterList_shouldReturnZeroList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [0;0]
			(ManageInventory.countEachLetter ['a';'b'] ['c';'d'] []);
	);
	"givenCharacterLetterList_shouldReturnOneAndZeroList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [1;0]
			(ManageInventory.countEachLetter ['a';'b'] ['c';'a';'d'] []);
	);
	"givenCharacterLetterList_shouldCountEachLetterCorrectly" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [1;2]
			(ManageInventory.countEachLetter ['a';'b'] ['b';'a';'b';'c'] []);
	);

	(* ManageInventory.findCount *)
	"givenEmptyList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.findCount [] [] 1);
	);
	"givenSingletonList_shouldReturnEmptyCharacter" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.findCount ['a'] [1] 2);
	);
	"givenSingletonList_shouldReturnCharacter" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 1
			(ManageInventory.findCount ['a'] [1] 1);
	);
	"givenCharacterList_shouldReturnFirstCharacterWithCount" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 1
			(ManageInventory.findCount ['a';'b';'c'] [2;1;1] 1);
	);
	"givenCharacterList_shouldReturnEmptyCharacter" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.findCount ['a';'b';'c'] [1;2;3] 4);
	);

	(* ManageInventory.findDoublesAndTriples *)
	"givenEmptyList_shouldReturnZeroTuple" >:: ( fun() ->
		assert_equal
			~printer: printIntegerTuple (0,0)
			(ManageInventory.findDoublesAndTriples []);
	);
	"givenSingletonList_shouldReturnZeroTuple" >:: ( fun() ->
		assert_equal
			~printer: printIntegerTuple (0,0)
			(ManageInventory.findDoublesAndTriples ['a']);
	);
	"givenCharacterList_shouldReturnZeroTuple" >:: ( fun() ->
		assert_equal
			~printer: printIntegerTuple (0,0)
			(ManageInventory.findDoublesAndTriples ['a';'b';'c']);
	);
	"givenCharacterList_shouldCountFirstDouble" >:: ( fun() ->
		assert_equal
			~printer: printIntegerTuple (1,0)
			(ManageInventory.findDoublesAndTriples ['a';'b';'a';'b']);
	);
	"givenCharacterList_shouldCountFirstTriple" >:: ( fun() ->
		assert_equal
			~printer: printIntegerTuple (0,1)
			(ManageInventory.findDoublesAndTriples ['a';'b';'a';'b';'a';'b']);
	);
	"givenCharacterList_shouldCountFirstDoubleAndTriple" >:: ( fun() ->
		assert_equal
			~printer: printIntegerTuple (1,1)
			(ManageInventory.findDoublesAndTriples ['a';'b';'a';'b';'d';'e';'d';'e';'d';'e']);
	);

	(* ManageInventory.calculateCheckSum *)
	"givenEmptyList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.calculateCheckSum [] (0,0));
	);
	"givenSingletonList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.calculateCheckSum [['a';'b';'a']] (0,0));
	);
	"givenSingletonList_shouldReturnOne" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 1
			(ManageInventory.calculateCheckSum [['a';'b';'a';'b';'b']] (0,0));
	);
	"givenNestedCharacterList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(ManageInventory.calculateCheckSum [['a';'b';'a']; ['a';'b';'a']] (0,0));
	);
	"givenNestedCharacterList_shouldReturnOne" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 1
			(ManageInventory.calculateCheckSum [['a';'b';'a']; ['b';'b';'b']] (0,0));
	);

	(* ManageInventory.manageInventory *)
	"givenInput_shouldReturnCorrectChecksum" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 12
			(ManageInventory.manageInventory "test-input.txt");
	);

	(* ManageInventory.findDifferenceBetweenTwoLetterLists *)
	"givenEmptyList_shouldReturnEmptyList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") []
			(ManageInventory.findDifferenceBetweenTwoLetterLists [] [] [] 1);
	);
	"givenSingletonList_shouldReturnEmptyList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") []
			(ManageInventory.findDifferenceBetweenTwoLetterLists ['a'] ['a'] [] 1);
	);
	"givenSingletonList_shouldReturnFirstPositionInList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [1]
			(ManageInventory.findDifferenceBetweenTwoLetterLists ['a'] ['b'] [] 1);
	);
	"givenCharacterList_shouldReturnEmptyList" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") []
			(ManageInventory.findDifferenceBetweenTwoLetterLists ['a';'b';'c'] ['a';'b';'c'] [] 1);
	);
	"givenCharacterList_shouldReturnSingleDifferencePoint" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [3]
			(ManageInventory.findDifferenceBetweenTwoLetterLists ['a';'b';'c'] ['a';'b';'d'] [] 1);
	);
	"givenCharacterList_shouldReturnMultipleDifferencePoints" >:: ( fun() ->
		assert_equal
			~printer: (printIntegerList "") [1;2;3]
			(ManageInventory.findDifferenceBetweenTwoLetterLists ['a';'b';'c'] ['d';'e';'f'] [] 1);
	);

	(* ManageInventory.findDifferenceBetweenLetterListAndOtherLetterLists *)
	"givenEmptyList_shouldReturnEmptyTuple" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple ([], [])
			(ManageInventory.findDifferenceBetweenLetterListAndOtherLetterLists [] ['a';'b';'c'] 100);
	);
	"givenSingletonList_shouldReturnEmptyTuple" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple ([], [])
			(ManageInventory.findDifferenceBetweenLetterListAndOtherLetterLists [['a';'b';'c']] ['a';'b';'c'] 1);
	);
	"givenSingletonList_shouldReturnDifferencePoints" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple (['a';'b';'c'], [2;3])
			(ManageInventory.findDifferenceBetweenLetterListAndOtherLetterLists [['a';'d';'e']] ['a';'b';'c'] 2);
	);
	"givenNestedCharacterList_shouldReturnEmptyTuple" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple ([], [])
			(ManageInventory.findDifferenceBetweenLetterListAndOtherLetterLists [['a';'b';'e'];['a';'w';'c']] ['a';'b';'c'] 2);
	);
	"givenNestedCharacterList_shouldReturnFirstOneWithDifferencePoints" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple (['a';'b';'c'], [2;3])
			(ManageInventory.findDifferenceBetweenLetterListAndOtherLetterLists [['a';'s';'e'];['f';'w';'c']] ['a';'b';'c'] 2);
	);

	(* ManageInventory.findDifferenceBetweenManyLetterLists *)
	"givenEmptyList_shouldReturnEmptyTuple" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple ([], [])
			(ManageInventory.findDifferenceBetweenManyLetterLists [] 99);
	);
	"givenSingletonList_shouldReturnEmptyTuple" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple ([], [])
			(ManageInventory.findDifferenceBetweenManyLetterLists [['a';'b';'c']] 1);
	);
	"givenNestedCharacterList_shouldReturnEmptyTuple" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple ([], [])
			(ManageInventory.findDifferenceBetweenManyLetterLists [['a';'b';'c'];['a';'v';'c']] 2);
	);
	"givenNestedCharacterList_shouldReturnFirstOneWithDifferencePoints" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple (['a';'b';'c'], [2])
			(ManageInventory.findDifferenceBetweenManyLetterLists [['a';'b';'c'];['a';'v';'c'];['a';'b';'r']] 1);
	);

	(* ManageInventory.findBoxId *)
	"givenInput_shouldReturnRightId" >:: ( fun() ->
		assert_equal
			~printer: printCharListIntegerListTuple (['f';'g';'h';'i';'j'], [3])
			(ManageInventory.findBoxId "part2-test-input.txt");
	);
];;

let _ = run_test_tt_main test_suite;;
