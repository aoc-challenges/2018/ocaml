(* Utility *)
let read_file filename =
	let lines = ref [] in
	let chan = open_in filename in
	try
	  while true; do
		lines := input_line chan :: !lines
	  done; !lines
	with End_of_file ->
	  close_in chan;
	  List.rev !lines;;

let explode s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) [];;

let rec explodeStrings stringList = match stringList with
	| [] -> []
	| s::stringListTail -> (explode s) :: (explodeStrings stringListTail);;

(* Helpers *)
let rec countLetters letter letterList count = match letterList with
	| [] -> count
	| l::letterListTail ->
		if l == letter
		then countLetters l letterListTail (count+1)
		else countLetters letter letterListTail count;;

let removeDuplicateLetters letterList = List.sort_uniq (compare) letterList;;

let rec countEachLetter uniqueLetterList letterList countList = match uniqueLetterList with
	| [] -> countList
	| l::uniqueLetterListTail ->
		let count = countLetters l letterList 0 in
		countEachLetter uniqueLetterListTail letterList (countList@[count]);;

let rec findCount letterList countList count = match letterList with
	| [] -> 0
	| _::letterListTail ->
		(match countList with
			| [] -> 0
			| n::countListTail ->
				if n == count
				then 1
				else (findCount letterListTail countListTail count));;

let findDoublesAndTriples letterList =
	let uniqueLetterList = removeDuplicateLetters letterList in
	let countList = countEachLetter uniqueLetterList letterList [] in
	((findCount letterList countList 2), (findCount letterList countList 3));;

let rec calculateCheckSum nestedLetterList (doubles, triples) = match nestedLetterList with
	| [] -> doubles * triples
	| letterList::nestedLetterListTail ->
		let (d, t) = findDoublesAndTriples letterList in
		calculateCheckSum nestedLetterListTail (doubles+d, triples+t);;

let rec findDifferenceBetweenTwoLetterLists letterList1 letterList2 indices index = match letterList1 with
	| [] -> indices
	| l1::letterListTail1 ->
		(match letterList2 with
			| [] -> indices
			| l2::letterListTail2 ->
				if l1 == l2
				then findDifferenceBetweenTwoLetterLists letterListTail1 letterListTail2 indices (index+1)
				else findDifferenceBetweenTwoLetterLists letterListTail1 letterListTail2 (indices@[index]) (index+1));;

let rec findDifferenceBetweenLetterListAndOtherLetterLists nestedLetterList letters numberOfDifferentIndices = match nestedLetterList with
	| [] -> ([], [])
	| letterList::nestedLetterListTail ->
		let differentIndices = findDifferenceBetweenTwoLetterLists letterList letters [] 1 in
		if List.length differentIndices == numberOfDifferentIndices
		then (letters, differentIndices)
		else findDifferenceBetweenLetterListAndOtherLetterLists nestedLetterListTail letters numberOfDifferentIndices;;

let rec findDifferenceBetweenManyLetterLists nestedLetterList numberOfDifferentIndices = match nestedLetterList with
	| [] -> ([], [])
	| [letterList] -> findDifferenceBetweenLetterListAndOtherLetterLists nestedLetterList letterList numberOfDifferentIndices
	| letterList::nestedLetterListTail ->
		let (letters, differentIndices) = findDifferenceBetweenLetterListAndOtherLetterLists nestedLetterListTail letterList numberOfDifferentIndices in
		if List.length differentIndices == numberOfDifferentIndices
		then (letters, differentIndices)
		else findDifferenceBetweenManyLetterLists nestedLetterListTail numberOfDifferentIndices;;

(* Puzzle *)
let manageInventory filename =
	let boxIds = read_file filename in
	let explodedBoxIds = explodeStrings boxIds in
	calculateCheckSum explodedBoxIds (0,0);;

let findBoxId filename =
	let boxIds = read_file filename in
	let explodedBoxIds = explodeStrings boxIds in
	findDifferenceBetweenManyLetterLists explodedBoxIds 1;;
