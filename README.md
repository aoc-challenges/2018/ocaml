# OCaml

## Using OCaml

Compile OCaml with

```ocaml -o hello hello.ml```

## Using OUnit as a test framework

Compile tests with

```ocamlfind ocamlc -o sampleTest -package oUnit -linkpkg -g Sample.ml SampleTest.ml```

Run tests with
```./sampleTest```
