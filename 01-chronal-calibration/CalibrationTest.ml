(* ocamlfind ocamlc -o test -package oUnit -linkpkg -g Calibration.ml CalibrationTest.ml *)

open OUnit;;

let test_suite = "CalibrationTest" >:::
[
	(* Calibration.sumAll *)
	"givenEmptyList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(Calibration.sumAll [] 0);
	);
	"givenSingletonList_shouldReturnSingleNumber" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 1
			(Calibration.sumAll [1] 0);
	);
	"givenNumberList_shouldAddAllNumbers" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 3
			(Calibration.sumAll [1;2] 0);
	);

	(* Calibration.calibrate *)
	"givenInput_shouldCalibrateFrequency" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 4
			(Calibration.calibrate "test-input.txt");
	);

	(* Calibration.findNumber *)
	"givenEmptyList_shouldReturnFalse" >:: ( fun() ->
		assert_equal
			~printer: string_of_bool false
			(Calibration.findNumber [] 1);
	);
	"givenSingletonList_whenMatchDoesNotExist_shouldReturnFalse" >:: ( fun() ->
		assert_equal
			~printer: string_of_bool false
			(Calibration.findNumber [2] 1);
	);
	"givenSingletonList_whenMatchExists_shouldReturnTrue" >:: ( fun() ->
		assert_equal
			~printer: string_of_bool true
			(Calibration.findNumber [1] 1);
	);
	"givenNumberList_whenMatchExists_shouldReturnTrue" >:: ( fun() ->
		assert_equal
			~printer: string_of_bool true
			(Calibration.findNumber [1;2] 1);
	);
	"givenNumberList_whenMatchDoesNotExist_shouldReturnFalse" >:: ( fun() ->
		assert_equal
			~printer: string_of_bool false
			(Calibration.findNumber [2;3] 1);
	);

	(* Calibration.findRepeatedSum *)
	"givenEmptyList_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(Calibration.findRepeatedSum [] []);
	);
	"givenSingletonList_whenTheValueIsZero_shouldReturnZero" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(Calibration.findRepeatedSum [0] []);
	);
	"givenSingletonList_whenTheValueIsNotZero_shouldThrowFailure" >:: ( fun() ->
		OUnit2.assert_raises
			(Failure "Will never repeat sum")
			(fun int -> Calibration.findRepeatedSum [3] []);
	);
	"givenNumberList_whenListIsNotCyclic_shouldFindRepeatedSum" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(Calibration.findRepeatedSum [1;-2;1] []);
	);
	"givenNumberList_whenListIsCyclic_shouldFindRepeatedSum" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 0
			(Calibration.findRepeatedSum [1;-2] []);
	);

	(* Calibration.advancedCalibrate *)
	"givenInput_shouldCalibrateFrequency" >:: ( fun() ->
		assert_equal
			~printer: string_of_int 10
			(Calibration.advancedCalibrate "test-input.txt");
	);
];;

let _ = run_test_tt_main test_suite;;
