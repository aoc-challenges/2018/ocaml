(* Utility *)
let read_file (filename:string): string list =
	let lines = ref [] in
	let chan = open_in filename in
	try
	  while true; do
		lines := input_line chan :: !lines
	  done; !lines
	with End_of_file ->
	  close_in chan;
	  List.rev !lines;;

let rec toNumberList (stringList:string list) (numberList:int list): int list = match stringList with
	| [] -> List.rev numberList
	| s::stringListTail -> toNumberList stringListTail ((int_of_string s)::numberList);;

(* Helpers *)
let rec sumAll (numberList:int list) (sum:int): int = match numberList with
	| [] -> sum
	| n::numberListTail -> sumAll numberListTail (sum + n);;

let rec findNumber (numberList:int list) (number:int): bool = match numberList with
	| [] -> false
	| n::numberListTail ->
		if number == n
		then true
		else findNumber numberListTail number;;

let rec findRepeatedSum (numberList:int list) (sumHistory:int list): int = match numberList with
	| [] | [0] -> 0
	| [n] -> failwith "Will never repeat sum"
	| n::numberListTail ->
		(match sumHistory with
		| [] -> findRepeatedSum (numberListTail@[n]) [n;0]
		| sum::_ ->
			let newSum = sum + n in
			let newSumHistory = newSum::sumHistory in
			try List.find (findNumber sumHistory) [newSum]
			with Not_found ->
				findRepeatedSum (numberListTail@[n]) newSumHistory);;

(* Puzzle *)
let calibrate (filename:string): int =
	let numbers = toNumberList (read_file filename) [] in
	sumAll numbers 0;;

let advancedCalibrate (filename:string): int =
	let numbers = toNumberList (read_file filename) [] in
	findRepeatedSum numbers [];;
